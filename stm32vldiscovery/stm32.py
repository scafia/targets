'''
File: app.py
Project: experiments
Created Date: Thursday December 10th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 5th November 2021 3:07:50 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

import serial
import time
from termcolor import colored, cprint

def bytes_from_size(sz):
    buffer = bytearray()
    buffer.append(sz >> 8 & 0xff)
    buffer.append(sz & 0xff)
    return bytes(buffer)

def size_from_bytes(sz_bytes):
    return sz_bytes[1] + (sz_bytes[0] << 8)


class STM32:

    BAUD_RATE = 115200

    def __init__(self, serial_port, debug=False, timeout=3):
        self.serial_handle = serial.Serial(serial_port, self.BAUD_RATE, timeout=timeout)
        self.debug = debug
        self.timeout = timeout

    # RAW SERIAL

    def send_bytes(self, bytes):
        self.serial_handle.write(bytes)

    def send_str(self, str):
        self.serial_handle.write(str.encode('utf-8'))

    def read_bytes(self, count):
        return self.serial_handle.read(count)

    def read_until(self, end_bytes):
        buffer = bytearray()

        start = time.time()
        
        while buffer.endswith(end_bytes) == False:
            now = time.time()
            if now-start > self.timeout:
                break
            buffer.extend(self.read_bytes(1))

        return bytes(buffer)

    

    # Higher level API

    def debug_read(self):
        while True:
            buffer = self.read_bytes(1)
            print("%c" %buffer[0], end="", flush=True)

    def wait_for_ack_or_nack(self, errorMessage=""):
        buf = self.read_until(b"\r\n")

        if buf.startswith(b"nack"):
            cprint(errorMessage + ": " + buf.decode("utf-8").strip(), 'red')
        elif self.debug:
            cprint("ack", 'green')

        return buf

    def test(self):
        self.send_bytes(b"t")
        if self.debug:
            cprint("Sending test command", 'yellow', end="... ")
        self.wait_for_ack_or_nack(errorMessage="Connection test failed")

    
    
