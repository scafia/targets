'''
File: main.py
Project: experiments
Created Date: Thursday December 10th 2020
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Friday, 5th November 2021 12:54:49 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2020 INRIA
'''

from stm32 import STM32
from benches.code.oscilloscopes.rto2xxx import Rto2xxx
from benches.code.helpers.ip_connection import IpConnection
import numpy as np

def read_whole_file(path):
    buffer = bytearray()
    with open(path, "rb") as f:
        byte = f.read(1)
        while byte != b"":
            buffer.append(byte[0])
            byte = f.read(1)

    # print(buffer.hex())
    return bytes(buffer)




if __name__ == '__main__':
    stm32 = STM32("/dev/ttyUSB0", debug=False)
    stm32.test()


